# j2paas 服务插件

### 一、介绍

通过自定义第3方服务插件，扩展系统的功能，下面开始介绍怎样编写J2PaaS插件。

### 二、服务插件

J2PaaS服务插件分为系统插件和项目插件，俩者生命周期不同，作用域也不同，系统插件是系统功能的扩展，例如可扩展RPC远程调用，使平台支持前后端分离和集群的能力等等，
项目插件只为项目服务，例如接入打卡设备，通过编写考勤机接口接入项目，实现考勤机指令的收发。

#### 1. 系统插件

系统插件是指随着平台的启动而启动，随着平台的结束而结束，它的生命周期与平台一致，为所有项目提供服务。

- 在pom.xml引入easyplatform-spi包

~~~xml

<dependency>
    <groupId>com.jikaiyun</groupId>
    <artifactId>easyplatform-spi</artifactId>
    <version>2.0.3</version>
</dependency>
~~~

- 扩展SPI接口 cn.easyplatform.spi.extension.ApplicationService:

~~~java
public interface ApplicationService {

    /**
     * 服务id
     *
     * @return
     */
    String getId();

    /**
     * 服务名称
     *
     * @return
     */
    String getName();

    /**
     * 服务描述
     *
     * @return
     */
    String getDescription();

    /**
     * 启动服务
     */
    void start();

    /**
     * 停止服务
     */
    void stop();

    /**
     * 获取运行时信息
     *
     * @return
     */
    Map<String, Object> getRuntimeInfo();

    /**
     * 服务状态
     *
     * @return
     */
    int getState();

    /**
     * 设置服务状态
     *
     * @param state
     */
    void setState(int state);

    /**
     * 启动时间
     *
     * @return
     */
    Date getStartTime();

    /**
     * 是否可以回调服务，如果为true，表示可以在逻辑中调用本实例
     *
     * @return
     */
    default boolean isCallable() {
        return false;
    }
}
~~~

- 插件注解 cn.easyplatform.spi.annotation.Plugin:

~~~java

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Plugin {
    String config() default "";
}
~~~

- 自定义插件必须扩展ApplicationService且要有Plugin的注解，Plugin主要目的是提供插件配置文件的名称，配置插件运行时需要的配置项，使用java标准的属性文件格式。

- 以easyplatform-mqtt插件为例：

~~~java
/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.mqtt;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.spi.annotation.Plugin;
import cn.easyplatform.spi.extension.ApplicationContext;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.StateType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * mqtt客户端接收信息和下发指令
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@Plugin(config = "easyplatform-mqtt.conf")
public class MqttService implements ApplicationService, MqttCallback {

    protected final static Logger log = LoggerFactory.getLogger(MqttService.class);

    private ApplicationContext ctx;

    private MqttClient client;

    // 启动时间
    private Date startTime;

    private int state = StateType.STOP;

    /**
     * 构造器，必须有ApplicationContext输入参数
     *
     * @param ctx
     */
    public MqttService(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    /**
     * 启动
     */
    public void start() {
        try {
            MemoryPersistence persistence = new MemoryPersistence();
            client = new MqttClient(ctx.getConfig("broker"), ctx.getConfig("clientId"), persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName(ctx.getConfig("userName"));
            options.setPassword(ctx.getConfig("password").toCharArray());
            options.setCleanSession(true);
            client.setCallback(this);
            client.connect(options);
            state = StateType.START;
            startTime = new Date();
        } catch (Exception ex) {
            if (log.isErrorEnabled())
                log.error("start", ex);
            throw new EasyPlatformRuntimeException("Could not create mqtt connection", ex);
        }
    }

    /**
     * 停止服务
     */
    public void stop() {
        if (state == StateType.START) {
            try {
                client.disconnect();
            } catch (MqttException e) {
                if (log.isErrorEnabled())
                    log.error("stop", e);
            }
        }
        state = StateType.STOP;
    }

    /**
     * 服务状态
     *
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * 服务id，必须是全局唯一，且必须保证在调用的逻辑中没有栏位或变量名称没有重复的
     *
     * @return
     */
    public String getId() {
        return "IotService";
    }

    public String getName() {
        return "电梯监控";
    }

    public String getDescription() {
        return "电梯监控";
    }

    public Date getStartTime() {
        return startTime;
    }

    /**
     * 运行时信息
     *
     * @return
     */
    public Map getRuntimeInfo() {
        //在这里可以增加一些信息，例如接收到几条，发送几条，成功多少，失败多少，以便在管理控制台显现
        return Collections.emptyMap();
    }

    /**
     * 服务状态
     *
     * @return
     */
    public int getState() {
        return state;
    }

    /**
     * 允许在逻辑中被调用，使用#id表示当前实例，#id.getState()表示获取当前服务状态
     *
     * @return
     */
    public boolean isCallable() {
        return true;
    }

    /**
     * 连接丢失的处理
     *
     * @param throwable
     */
    @Override
    public void connectionLost(Throwable throwable) {
        if (log.isErrorEnabled())
            log.error("connectionLost", throwable);
        try {
            client.reconnect();
        } catch (MqttException e) {
            if (log.isErrorEnabled())
                log.error("reconnect", throwable);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException interruptedException) {
            }
        }
    }

    /**
     * 发布指令
     *
     * @param msg
     */
    public void publish(Object msg) {
        publish(ctx.getConfig("pubTopic"), msg, NumberUtils.toInt(ctx.getConfig("qos")), false);
    }

    /**
     * 发送指令
     *
     * @param topic
     * @param msg
     * @param qos
     * @param retained
     */
    public void publish(String topic, Object msg, int qos, boolean retained) {
        MqttMessage message = new MqttMessage();
        message.setQos(qos);
        message.setRetained(retained);
        try {
            if (msg instanceof String)
                message.setPayload(((String) msg).getBytes("utf-8"));
            else if (msg instanceof byte[])
                message.setPayload((byte[]) msg);
            else if (msg instanceof InputStream)
                message.setPayload(IOUtils.toByteArray((InputStream) msg));
            else throw new EasyPlatformRuntimeException("Unsupported yet");
            client.publish(topic, message);
        } catch (MqttException e) {
            throw new EasyPlatformRuntimeException(e);
        } catch (IOException e) {
            throw new EasyPlatformRuntimeException(e);
        }
    }

    /**
     * 接收消息
     *
     * @param topic
     * @param mqttMessage
     * @throws Exception
     */
    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        if (log.isDebugEnabled())
            log.debug(String.format(" messageArrived:%s,%d", topic, mqttMessage.getQos()));
        Map<String, Object> data = new HashMap<>(4);
        data.put("topic", topic);
        data.put("qos", mqttMessage.getQos());
        data.put("payload", mqttMessage.getPayload());
        data.put("msgId", mqttMessage.getId());
        ctx.getProjectContext(ctx.getConfig("projectId")).asyncLogic(this, ctx.getConfig("logicId"), data);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
~~~

***请注意***，实现类的构造函数传参必须是cn.easyplatform.spi.extension.ApplicationContext，ApplicationContext主要提供以下方法：

~~~java
public interface ApplicationContext {

    /**
     * 通过项目id获取项目实例
     *
     * @return
     */
    ProjectContext getProjectContext(String projectId);

    /**
     * @param name
     * @return
     */
    String getConfig(String name);

    /**
     * 配置项名称
     *
     * @return
     */
    Set<String> keys();

    /**
     * 获取平台配置
     *
     * @param name
     * @return
     */
    Map<String, String> getApplicationConfig(String name);

    /**
     * 获取平台指定的配置项
     *
     * @param group
     * @param name
     * @return
     */
    String getApplicationConfigValue(String group, String name);
}
~~~

- 配置文件easyplatform-mqtt.conf,文件可以在系统的配置文件目录，也可以打包在自身jar中，如果都存在，以配置文件夹的优先。

~~~properties
#mqtt服务器地址
broker=tcp://127.0.0.1:1883
#客户端唯一标识
clientId=emq-client
#连接用户
userName=test
#连接密码
password=test
#订阅的通道名称
subTopic=testtopic/#
#发送的通道名称
pubTopic=testtopic/1
#qos编号
qos=2
#对应的项目id
projectId=demo
#要调用的处理逻辑
logicId=testlogic
~~~

- 执行maven install，把打好的包上传至第3方库目录，系统会自动热加载，如果是分布式部署，系统通过RPC自动分发到各个节点，由节点自动加载到JVM。

#### 2.项目插件

    项目插件是指由项目配置参数，使用参数来定义插件，它的生命周期与项目一致，只为项目本身提供服务，在多项目系统中，每个项目的插件服务是相互独立。

- 还是以mqtt为例，登录J2PaaS开发具，新建Java类，如图所示：

![meta.png](img/plugin/1632813867.jpg)

- 登录J2PaaS前端管理控制台，选择"系统设置"菜单，点击“自定义服务”，弹出自定义服务窗口，如图所示：

![meta.png](img/plugin/1632814241.jpg)

- 点击查询按钮，选择刚才开发工具新建的MqttService记录，在这里可以进一步编辑，然后保存，如果有其它的依赖项，请预先上传到第3方库，否则会编译不成功。


- 保存之后，请选择“自定义服务”菜单，在显示的功能页面列表中选中MqttService，点击“启动服务”按钮，项目启动MqttService服务，并显示启动结果及运行日志,如图：

![meta.png](img/plugin/1632815041.jpg)

- 如果有细心的朋友会发现，项目插件的构造函数传参与系统插件的传参是不一样的，也不使用Plugin注解，项目插件使用的传参是cn.easyplatform.spi.extension.ProjectContext类：

~~~java
public interface ProjectContext {

    /**
     * 项目id
     *
     * @return
     */
    String getId();

    /**
     * 项目名称
     *
     * @return
     */
    String getName();

    /**
     * easyplatform工作空间
     *
     * @return
     */
    String getWorkspace();

    /**
     * 当前项目工作空间
     *
     * @return
     */
    String getWorkPath();

    /**
     * $app/WEB-INF/
     *
     * @return
     */
    String getHome();

    /**
     * 获取主数据库的连接
     *
     * @return
     */
    DataSource getDataSource();

    /**
     * 获取指定数据库id的连接
     *
     * @return
     */
    DataSource getDataSource(String dbId);

    /**
     * 获取在线用户数
     *
     * @return
     */
    int getUserCount();

    /**
     * 获取配置信息
     *
     * @param name
     * @return
     */
    Map<String, String> getConfig(String name);

    /**
     * 获取配置项信息
     *
     * @param name
     * @return
     */
    String getConfigValue(String name);

    /**
     * 发布消息
     *
     * @param topic
     * @param data
     * @param toUsers
     */
    void publish(String topic, Serializable data, String... toUsers);

    /**
     * 执行功能
     *
     * @param id     功能参数id
     * @param inputs 输入参数
     * @return 返回执行结果
     */
    IResponseMessage<?> executeTask(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 异步执行功能或逻辑
     *
     * @param id     功能参数id
     * @param inputs 输入参数
     * @return 返回Future对象
     */
    Future<IResponseMessage<?>> asyncTask(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 执行逻辑
     *
     * @param id     逻辑参数id
     * @param inputs 输入参数
     * @return 返回执行结果
     */
    void executeLogic(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 异步执行逻辑
     *
     * @param id     逻辑参数id
     * @param inputs 输入参数
     */
    void asyncLogic(ApplicationService service, String id, Map<String, Object> inputs);
}
~~~

通过提供这些接口，插件可以获取项目的配置项、执行功能、执行逻辑以及数据源等信息。

**请注意：** 动态编译需要JDK运行时，不能使用JRE!

