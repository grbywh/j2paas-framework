/**
* Here's the mold file , a mold means a HTML struct that the widget really presented.
* yep, we build html in Javascript , that make it more clear and powerful.
*/
function (out) {

	/*
		class="${zcls} ${this.getSclass()}" id="${uuid}"
	*/

    out.push('<div ', this.domAttrs_() ,'>',
        '<div class="input-group date form_date">' ,
        '<input input="start" class="form-control old-input" size="16" type="text" placeholder="',this._placeholder,'" value="',this._value,'"/>' ,
        '<span class="input-group-addon old-addon">' ,
        '<span class="glyphicon glyphicon-calendar old-glyphicon"/>' ,
        '</span>' ,
        '</div>' ,
        '</div>');


}

