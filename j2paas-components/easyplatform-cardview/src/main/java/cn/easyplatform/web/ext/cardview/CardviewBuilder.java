/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.cardview;

import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.Widget;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import java.util.ArrayList;
import java.util.List;

public class CardviewBuilder implements ComponentBuilder, Widget {

    private ComponentHandler dataHandler;

    private Cardview cardview;

    public CardviewBuilder(ComponentHandler dataHandler, Cardview cardview) {
        this.dataHandler = dataHandler;
        this.cardview = cardview;
    }


    public Component build() {
        cardview.setAttribute("$proxy", this);
        create(cardview,true);
        return cardview;
    }


    private void create(Cardview cbx,boolean addEvtFlag) {

        if(cbx.getSequence()!=null && cbx.getSequence().length != 0) {

            int imgNum = getIndex(cbx.getSequence(), "img");
            int authorNum = getIndex(cbx.getSequence(), "author");
            int vardateNum = getIndex(cbx.getSequence(), "vardate");
            int portraitNum = getIndex(cbx.getSequence(), "portrait");
            int headerNum = getIndex(cbx.getSequence(), "header");
            int flagcontentNum = getIndex(cbx.getSequence(), "flagcontent");
            int flagcolourNum = getIndex(cbx.getSequence(), "flagcolour");
            int frontevtNum = getIndex(cbx.getSequence(), "frontevt");
            int centreevtNum = getIndex(cbx.getSequence(), "centreevt");
            int tailevtNum = getIndex(cbx.getSequence(), "tailevt");
            int contentevtNum = getIndex(cbx.getSequence(), "contentevt");
            int fronturlNum = getIndex(cbx.getSequence(), "fronturl");
            int centreurlNum = getIndex(cbx.getSequence(), "centreurl");
            int tailurlNum = getIndex(cbx.getSequence(), "tailurl");
            int contenturlNum = getIndex(cbx.getSequence(), "contenturl");
            int contentaNum = getIndex(cbx.getSequence(), "contenta");
            int contentbNum = getIndex(cbx.getSequence(), "contentb");
            int contentcNum = getIndex(cbx.getSequence(), "contentc");
            int contentdNum = getIndex(cbx.getSequence(), "contentd");
            int contenteNum = getIndex(cbx.getSequence(), "contente");
            int msgDateNum = getIndex(cbx.getSequence(), "msgDate");
            int msgStatusNum = getIndex(cbx.getSequence(), "msgStatus");

            if (null != cbx.getQuery() && !"".equals(cbx.getQuery())) {
                List<?> data = dataHandler.selectList0(Object[].class, cbx.getDbId(), cbx.getQuery());

                List<String> imgStr = new ArrayList<>();
                List<String> authorStr = new ArrayList<>();
                List<String> vardateStr = new ArrayList<>();
                List<String> portraitStr = new ArrayList<>();
                List<String> headerStr = new ArrayList<>();
                List<String> flagcontentStr = new ArrayList<>();
                List<String> flagcolourStr = new ArrayList<>();
                List<String> frontevtStr = new ArrayList<>();
                List<String> centreevtStr = new ArrayList<>();
                List<String> tailevtStr = new ArrayList<>();
                List<String> contentevtStr = new ArrayList<>();
                List<String> fronturlStr = new ArrayList<>();
                List<String> centreurlStr = new ArrayList<>();
                List<String> tailurlStr = new ArrayList<>();
                List<String> contenturlStr = new ArrayList<>();
                List<String> contentaStr = new ArrayList<>();
                List<String> contentbStr = new ArrayList<>();
                List<String> contentcStr = new ArrayList<>();
                List<String> contentdStr = new ArrayList<>();
                List<String> contenteStr = new ArrayList<>();
                List<String> msgDateStr = new ArrayList<>();
                List<String> msgStatusStr = new ArrayList<>();

                if (data != null && !data.isEmpty()) {
                    for (int i = 0; i < data.size(); i++) {
                        Object[] obj = (Object[]) data.get(i);

                        if (imgNum != -1 && obj.length > imgNum) {
                            imgStr.add((String) obj[imgNum]);
                        }
                        if (authorNum != -1 && obj.length > authorNum) {
                            authorStr.add((String)obj[authorNum]);
                        }
                        if (vardateNum != -1 && obj.length > vardateNum) {
                            vardateStr.add((String) obj[vardateNum]);
                        }
                        if (portraitNum != -1 && obj.length > portraitNum) {
                            portraitStr.add((String) obj[portraitNum]);
                        }
                        if (headerNum != -1 && obj.length > headerNum) {
                            headerStr.add((String) obj[headerNum]);
                        }
                        if (flagcontentNum != -1 && obj.length > flagcontentNum) {
                            flagcontentStr.add((String) obj[flagcontentNum]);
                        }
                        if (flagcolourNum != -1 && obj.length > flagcolourNum) {
                            flagcolourStr.add((String) obj[flagcolourNum]);
                        }
                        if (frontevtNum != -1 && obj.length > frontevtNum) {
                            frontevtStr.add((String) obj[frontevtNum]);
                        }
                        if (centreevtNum != -1 && obj.length > centreevtNum) {
                            centreevtStr.add((String) obj[centreevtNum]);
                        }
                        if (tailevtNum != -1 && obj.length > tailevtNum) {
                            tailevtStr.add((String) obj[tailevtNum]);
                        }
                        if (fronturlNum != -1 && obj.length > fronturlNum) {
                            fronturlStr.add((String) obj[fronturlNum]);
                        }
                        if (centreurlNum != -1 && obj.length > centreurlNum) {
                            centreurlStr.add((String) obj[centreurlNum]);
                        }
                        if (tailurlNum != -1 && obj.length > tailurlNum) {
                            tailurlStr.add((String) obj[tailurlNum]);
                        }
                        if (contentaNum != -1 && obj.length > contentaNum) {
                            contentaStr.add((String) obj[contentaNum]);
                        }
                        if (contentbNum != -1 && obj.length > contentbNum) {
                            contentbStr.add((String) obj[contentbNum]);
                        }
                        if (contentcNum != -1 && obj.length > contentcNum) {
                            contentcStr.add((String) obj[contentcNum]);
                        }
                        if (contentdNum != -1 && obj.length > contentdNum) {
                            contentdStr.add((String) obj[contentdNum]);
                        }
                        if (contenteNum != -1 && obj.length > contenteNum) {
                            contenteStr.add((String) obj[contenteNum]);
                        }
                        if (contentevtNum != -1 && obj.length > contentevtNum) {
                            contentevtStr.add((String) obj[contentevtNum]);
                        }
                        if (contenturlNum != -1 && obj.length > contenturlNum) {
                            contenturlStr.add((String) obj[contenturlNum]);
                        }
                        if (msgDateNum != -1 && obj.length > msgDateNum) {
                            msgDateStr.add((String) obj[msgDateNum]);
                        }
                        if (msgStatusNum != -1 && obj.length > msgStatusNum) {
                            msgStatusStr.add((String) obj[msgStatusNum]);
                        }

                    }
                }else{
                    cbx.setContenta("['none']");
                }

                if (imgStr.size() > 0) {
                    cbx.setImg(JSON.toJSONString(imgStr));
                }
                if (authorStr.size() > 0) {
                    cbx.setAuthor(JSON.toJSONString(authorStr));
                }
                if (vardateStr.size() > 0) {
                    cbx.setVardate(JSON.toJSONString(vardateStr));
                }
                if (portraitStr.size() > 0) {
                    cbx.isPortraitflag(true);
                    cbx.setPortrait(JSON.toJSONString(portraitStr));
                }
                if (frontevtStr.size() > 0) {
                    cbx.setFrontevt(JSON.toJSONString(frontevtStr));
                }
                if (centreevtStr.size() > 0) {
                    cbx.setCentreevt(JSON.toJSONString(centreevtStr));
                }
                if (tailevtStr.size() > 0) {
                    cbx.setTailevt(JSON.toJSONString(tailevtStr));
                }
                if (headerStr.size() > 0) {
                    cbx.setHeader(JSON.toJSONString(headerStr));
                }
                if (flagcontentStr.size() > 0) {
                    cbx.setFlagcontent(JSON.toJSONString(flagcontentStr));
                }
                if (flagcolourStr.size() > 0) {
                    cbx.setFlagcolour(JSON.toJSONString(flagcolourStr));
                }
                if (centreurlStr.size() > 0) {
                    cbx.setHeader(JSON.toJSONString(centreurlStr));
                }
                if (fronturlStr.size() > 0) {
                    cbx.setFlagcontent(JSON.toJSONString(fronturlStr));
                }
                if (tailurlStr.size() > 0) {
                    cbx.setFlagcolour(JSON.toJSONString(tailurlStr));
                }
                if (contentaStr.size() > 0) {
                    cbx.setContenta(JSON.toJSONString(contentaStr));
                }
                if (contentbStr.size() > 0) {
                    cbx.setContentb(JSON.toJSONString(contentbStr));
                }
                if (contentcStr.size() > 0) {
                    cbx.setContentc(JSON.toJSONString(contentcStr));
                }
                if (contentdStr.size() > 0) {
                    cbx.setContentd(JSON.toJSONString(contentdStr));
                }
                if (contenteStr.size() > 0) {
                    cbx.setContente(JSON.toJSONString(contenteStr));
                }
                if (contentevtStr.size() > 0) {
                    cbx.setContentevt(JSON.toJSONString(contentevtStr));
                }
                if (contenturlStr.size() > 0) {
                    cbx.setContenturl(JSON.toJSONString(contenturlStr));
                }
                if (msgDateStr.size() > 0) {
                    cbx.setMsgDate(JSON.toJSONString(msgDateStr));
                }
                if (msgStatusStr.size() > 0) {
                    cbx.setMsgStatus(JSON.toJSONString(msgStatusStr));
                }
            }
        }
        if(cbx.getEvent()!=null && cbx.getEvent()!="" && addEvtFlag==true) {
            dataHandler.addEventListener("onFrontLink", cbx);
            dataHandler.addEventListener("onCentreLink", cbx);
            dataHandler.addEventListener("onTailLink", cbx);

            if(cbx.getExtraEvent()==null && cbx.getExtraEvent()=="") {
                dataHandler.addEventListener("onTouch", cbx);
            }
        }
        if(cbx.getExtraEvent()!=null && cbx.getExtraEvent()!=""&& addEvtFlag==true){
            dataHandler.addEventListener("onTouch", cbx, cbx.getExtraEvent());
        }
    }


    public void reload(Component widget) {
        Cardview cbx = (Cardview)widget;
        this.create(cbx,false);
        cbx.setReloadFlag(true);
    }

    public int getIndex(String[] array,String value){
        for(int i = 0;i<array.length;i++){
            if(array[i].equals(value)){
                return i;
            }
        }
        return -1;
    }


}
