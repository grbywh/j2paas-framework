/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package test.ctrl;

import cn.easyplatform.Introcoach;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkmax.zul.Coachmark;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;


public class DemoWindowComposer extends SelectorComposer {
	
	/*@Wire
	private Introcoach myComp;*/
	@Wire
	private Introcoach myComp1;
	@Wire
	private Button buttonNext;
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		/*myComp.close();
		Label label = new Label("12313");
		myComp.setTarget("button_1");
		myComp.appendChild(label);
		myComp.getActionTargetUUID("button_1");
		myComp.addEventListener(Introcoach.ON_UUID, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (event.getName().equals(Introcoach.ON_UUID)) {
					String uuid = (String) event.getData();
					System.out.println(uuid);
				}
			}
		});
		myComp.open();*/
		myComp1.close();
		Label label = new Label("12313");
		myComp1.appendChild(label);
		myComp1.setTarget("button_1");
		myComp1.setPosition("after_center");
		myComp1.open();
		myComp1.getActionTargetUUID("button_1");
		myComp1.addEventListener(Introcoach.ON_UUID, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (event.getName().equals(Introcoach.ON_UUID)) {
					String uuid = (String) event.getData();
					System.out.println(uuid);
				}
			}
		});
		buttonNext.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				myComp1.close();
				/*Label label = new Label("321321");
				myComp1.appendChild(label);
				myComp1.setTarget("button_2");
				myComp1.setPosition("after_center");
				myComp1.open();
				myComp1.getActionTargetUUID("button_2");
				myComp1.addEventListener(Introcoach.ON_UUID, new EventListener<Event>() {
					@Override
					public void onEvent(Event event) throws Exception {
						if (event.getName().equals(Introcoach.ON_UUID)) {
							String uuid = (String) event.getData();
							System.out.println(uuid);
						}
					}
				});*/
			}
		});
		/*"left-top-aligned"，"top", "right-top-aligned",
				"left", "center"，"right"，
		"left-bottom-aligned","bottom" ,"right-bottom-aligned"*/
		myComp1.setSite("center");
	}
	
	public void onFoo$myComp (ForwardEvent event) {
		Event mouseEvent = (Event) event.getOrigin();
		alert("You listen onFoo: " + mouseEvent.getTarget());
	}
}