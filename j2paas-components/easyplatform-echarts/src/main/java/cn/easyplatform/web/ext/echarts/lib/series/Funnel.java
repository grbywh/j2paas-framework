/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.series.base.LabelLine;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Funnel extends Series {
    private Integer min;
    private Integer max;
    private String minSize;
    private String maxSize;
    private String sort;
    private Integer gap;
    private Boolean legendHoverLink;
    private String funnelAlign;
    private LabelLine labelLine;
    private Object width;
    private Object height;
    private Object left;
    private Object top;
    private Object right;
    private Object bottom;
    private Object center;

    public Object center() {
        return this.center;
    }

    public Funnel center(Object value) {
       if (value instanceof String)
            this.center = ((String) value).split(",");
        else
            this.center = center;
        return this;
    }

    public Object left() {
        return this.left;
    }

    public Funnel left(Object left) {
        this.left = left;
        return this;
    }

    public Funnel left(Integer left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public Funnel top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public Funnel right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public Funnel bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public Funnel width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Funnel height(Object height) {
        this.height = height;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Funnel() {
        type = "funnel";
    }

    public Object min() {
        return min;
    }

    public Funnel min(Integer min) {
        this.min = min;
        return this;
    }

    public Object max() {
        return max;
    }

    public Funnel max(Integer max) {
        this.max = max;
        return this;
    }

    public Object minSize() {
        return minSize;
    }

    public Funnel minSize(String minSize) {
        this.minSize = minSize;
        return this;
    }

    public Object maxSize() {
        return maxSize;
    }

    public Funnel maxSize(String maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public String sort() {
        return sort;
    }

    public Funnel sort(String sort) {
        this.sort = sort;
        return this;
    }

    public Object gap() {
        return gap;
    }

    public Funnel gap(Integer gap) {
        this.gap = gap;
        return this;
    }

    public Object legendHoverLink() {
        return legendHoverLink;
    }

    public Funnel legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Object funnelAlign() {
        return funnelAlign;
    }

    public Funnel funnelAlign(String funnelAlign) {
        this.funnelAlign = funnelAlign;
        return this;
    }

    public LabelLine labelLine() {
        if (labelLine == null)
            labelLine = new LabelLine();
        return labelLine;
    }

    public Funnel labelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
        return this;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public String getMinSize() {
        return minSize;
    }

    public void setMinSize(String minSize) {
        this.minSize = minSize;
    }

    public String getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(String maxSize) {
        this.maxSize = maxSize;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getGap() {
        return gap;
    }

    public void setGap(Integer gap) {
        this.gap = gap;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public String getFunnelAlign() {
        return funnelAlign;
    }

    public void setFunnelAlign(String funnelAlign) {
        this.funnelAlign = funnelAlign;
    }

    public LabelLine getLabelLine() {
        return labelLine;
    }

    public void setLabelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
    }
}
