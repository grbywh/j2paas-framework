/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;


import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ThemeRiver extends Point {
    private String type = "themeRiver";
    private String coordinateSystem;
    private Object[] boundaryGap;
    private Integer singleAxisIndex;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private Emphasis emphasis;
    private Object legend;
    private Tooltip tooltip;
    private List<Object> data;
    private Object width;
    private Object height;

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public ThemeRiver emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Object height() {
        return height;
    }

    public ThemeRiver height(Object height) {
        this.height = height;
        return this;
    }

    public Object width() {
        return width;
    }

    public ThemeRiver width(Object width) {
        this.width = width;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public ThemeRiver data(Object... values) {
        if (data == null)
            data = new ArrayList<Object>();
        this.data.add(values);
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return tooltip;
    }

    public ThemeRiver tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public String type() {
        return type;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public ThemeRiver coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Object[] boundaryGap() {
        return boundaryGap;
    }

    public ThemeRiver boundaryGap(Object x, Object y) {
        this.boundaryGap = new Object[]{x, y};
        return this;
    }

    public Integer singleAxisIndex() {
        return singleAxisIndex;
    }

    public ThemeRiver singleAxisIndex(Integer singleAxisIndex) {
        this.singleAxisIndex = singleAxisIndex;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return label;
    }

    public ThemeRiver label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public ThemeRiver itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Object legend() {
        return legend;
    }

    public ThemeRiver legend(Object legend) {
        this.legend = legend;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Object[] getBoundaryGap() {
        return boundaryGap;
    }

    public void setBoundaryGap(Object[] boundaryGap) {
        this.boundaryGap = boundaryGap;
    }

    public Integer getSingleAxisIndex() {
        return singleAxisIndex;
    }

    public void setSingleAxisIndex(Integer singleAxisIndex) {
        this.singleAxisIndex = singleAxisIndex;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Object getLegend() {
        return legend;
    }

    public void setLegend(Object legend) {
        this.legend = legend;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }
}
