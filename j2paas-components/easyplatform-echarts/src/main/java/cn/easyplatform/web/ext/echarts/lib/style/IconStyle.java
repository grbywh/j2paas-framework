/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import java.io.Serializable;

public class IconStyle implements Serializable {
    /**
     * 颜色
     */
    private String color;
    /**
     * 标题边框颜色
     */
    private String borderColor;
    /**
     * borderWidth
     */
    private Integer borderWidth;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果
     */
    private Integer shadowBlur;
    /**
     * 阴影颜色
     */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离
     */
    private Integer shadowOffsetX;
    /**
     * 阴影垂直方向上的偏移距离
     */
    private Integer shadowOffsetY;

    private Object opacity;

    private String borderType;
    /**
     * 文本位置，'left' / 'right' / 'top' / 'bottom'。
     */
    private String textPosition;
    /**
     * 文本颜色，如果未设定，则依次取图标 emphasis 时候的填充色、描边色，如果都不存在，则为 '#000'。
     */
    private String textFill;
    /**
     * 文本对齐方式，'left' / 'center' / 'right'。
     */
    private String textAlign;
    /**
     * 文本区域填充色。
     */
    private String textBackgroundColor;
    /**
     * 文本区域圆角大小。
     */
    private Object textBorderRadius;
    /**
     * 文本区域内边距。
     */
    private Object textPadding;

    public Object textPadding() {
        return this.textPadding;
    }

    public IconStyle textPadding(Object textPadding) {
        this.textPadding = textPadding;
        return this;
    }

    public Object textBorderRadius() {
        return this.textBorderRadius;
    }

    public IconStyle textBorderRadius(Object textBorderRadius) {
        this.textBorderRadius = textBorderRadius;
        return this;
    }

    public String textBackgroundColor() {
        return this.textBackgroundColor;
    }

    public IconStyle textBackgroundColor(String textBackgroundColor) {
        this.textBackgroundColor = textBackgroundColor;
        return this;
    }

    public String textAlign() {
        return this.textAlign;
    }

    public IconStyle textAlign(String textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    public String textFill() {
        return this.textFill;
    }

    public IconStyle textFill(String textFill) {
        this.textFill = textFill;
        return this;
    }

    public String textPosition() {
        return this.textPosition;
    }

    public IconStyle textPosition(String textPosition) {
        this.textPosition = textPosition;
        return this;
    }

    public String borderType() {
        return this.borderType;
    }

    public IconStyle borderType(String borderType) {
        this.borderType = borderType;
        return this;
    }

    public Object opacity() {
        return this.opacity;
    }

    public IconStyle opacity(Object opacity) {
        this.opacity = opacity;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public IconStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public IconStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    public IconStyle shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public String shadowColor() {
        return this.shadowColor;
    }

    public IconStyle shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Integer shadowOffsetX() {
        return this.shadowOffsetX;
    }

    public IconStyle shadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    public Integer shadowOffsetY() {
        return this.shadowOffsetY;
    }

    public IconStyle shadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }

    /**
     * 获取color值
     */
    public String color() {
        return this.color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public IconStyle color(String color) {
        this.color = color;
        return this;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public Object getOpacity() {
        return opacity;
    }

    public void setOpacity(Object opacity) {
        this.opacity = opacity;
    }

    public String getBorderType() {
        return borderType;
    }

    public void setBorderType(String borderType) {
        this.borderType = borderType;
    }

    public String getTextPosition() {
        return textPosition;
    }

    public void setTextPosition(String textPosition) {
        this.textPosition = textPosition;
    }

    public String getTextFill() {
        return textFill;
    }

    public void setTextFill(String textFill) {
        this.textFill = textFill;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public String getTextBackgroundColor() {
        return textBackgroundColor;
    }

    public void setTextBackgroundColor(String textBackgroundColor) {
        this.textBackgroundColor = textBackgroundColor;
    }

    public Object getTextBorderRadius() {
        return textBorderRadius;
    }

    public void setTextBorderRadius(Object textBorderRadius) {
        this.textBorderRadius = textBorderRadius;
    }

    public Object getTextPadding() {
        return textPadding;
    }

    public void setTextPadding(Object textPadding) {
        this.textPadding = textPadding;
    }
}
