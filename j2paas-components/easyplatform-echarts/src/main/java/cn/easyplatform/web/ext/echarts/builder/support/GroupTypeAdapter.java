/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder.support;

import cn.easyplatform.web.ext.echarts.lib.graphic.*;
import cn.easyplatform.web.ext.echarts.lib.series.Graph;
import com.google.gson.*;

import java.lang.reflect.Type;

public class GroupTypeAdapter implements JsonDeserializer<Graphic[]>, JsonSerializer<Graphic[]> {
    @Override
    public Graphic[] deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonArray array = (JsonArray) jsonElement;
        Graphic[] result = new Graphic[array.size()];
        for (int i = 0; i < result.length; i++) {
            JsonObject jsonObject = (JsonObject) array.get(i);
            String tp = jsonObject.get("type").getAsString();
            switch (tp) {
                case "image":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Image.class);
                    break;
                case "text":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Text.class);
                    break;
                case "rect":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Rect.class);
                    break;
                case "circle":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Circle.class);
                    break;
                case "ring":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Ring.class);
                    break;
                case "sector":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Sector.class);
                    break;
                case "arc":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Arc.class);
                    break;
                case "polygon":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Polygon.class);
                    break;
                case "polyline":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Polyline.class);
                    break;
                case "line":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Line.class);
                    break;
                case "graph":
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, Graph.class);
                default:
                    result[i] = jsonDeserializationContext.deserialize(jsonObject, BezierCurve.class);
            }
        }
        return result;
    }

    @Override
    public JsonElement serialize(Graphic[] graphics, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonArray array = new JsonArray();
        for (int i = 0; i < graphics.length; i++) {
            switch (graphics[i].getType()) {
                case "image":
                    array.add(jsonSerializationContext.serialize(graphics[i], Image.class));
                    break;
                case "text":
                    array.add(jsonSerializationContext.serialize(graphics[i], Text.class));
                    break;
                case "rect":
                    array.add(jsonSerializationContext.serialize(graphics[i], Rect.class));
                    break;
                case "circle":
                    array.add(jsonSerializationContext.serialize(graphics[i], Circle.class));
                    break;
                case "ring":
                    array.add(jsonSerializationContext.serialize(graphics[i], Ring.class));
                    break;
                case "sector":
                    array.add(jsonSerializationContext.serialize(graphics[i], Sector.class));
                    break;
                case "arc":
                    array.add(jsonSerializationContext.serialize(graphics[i], Arc.class));
                    break;
                case "polygon":
                    array.add(jsonSerializationContext.serialize(graphics[i], Polygon.class));
                    break;
                case "polyline":
                    array.add(jsonSerializationContext.serialize(graphics[i], Polyline.class));
                    break;
                case "line":
                    array.add(jsonSerializationContext.serialize(graphics[i], Line.class));
                    break;
                default:
                    array.add(jsonSerializationContext.serialize(graphics[i], BezierCurve.class));
            }
        }
        return array;
    }
}
