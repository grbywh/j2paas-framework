/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder.impl;

import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.echarts.ECharts;
import cn.easyplatform.web.ext.echarts.builder.TemplateUnit;
import cn.easyplatform.web.ext.echarts.lib.Option;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GaugeChartsBuilder extends AbstractChartsBuilder {

    public GaugeChartsBuilder(String type) {
        super(type);
    }

    @Override
    protected void setSeries(Option option, String name, JsonArray data) {
            super.setSeries(option, name, data);
    }

    @Override
    public void build(ECharts charts, ComponentHandler dataHandler) {
        if (dataHandler == null) {
            build(charts.getOption(), getTemplate(charts.getTemplate()), true);
        } else if (charts.getQuery() != null) {
            List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) charts.getQuery());
            createModel(charts, data);
        } else {
            if (charts.getOption().getSeries() instanceof Map) {//单个仪表盘
                Map<String, Object> gauge = (Map<String, Object>) charts.getOption().getSeries();
                if (gauge.get("query") != null) {
                    List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) gauge.get("query"));
                    createModel(charts, data);
                }
            } else if (charts.getOption().getSeries() instanceof List) {
                List<?> series = (List<?>) charts.getOption().getSeries();
                if (series.size() == 1) {//单个仪表盘
                    Map<String, Object> gauge = (Map<String, Object>) series.get(0);
                    if (gauge.get("query") != null) {
                        List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) gauge.get("query"));
                        createModel(charts, data);
                    }
                } else {
                    //多个仪表盘
                    Set<Object> legend = new HashSet<>();
                    for (Object serie : series) {
                        Map<String, Object> gauge = (Map<String, Object>) serie;
                        if (gauge.get("query") != null) {
                            List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) gauge.remove("query"));
                            List<Object> model = new ArrayList<>();
                            for (Object[] objs : data) {
                                Map<String, Object> row = new HashMap<>();
                                row.put("name", objs[0]);
                                row.put("value", objs[1]);
                                model.add(row);
                                legend.add(objs[0]);
                            }
                            gauge.put("data", model);
                        }
                    }
                    if (charts.getOption().getLegend() != null)
                        charts.getOption().getLegend().setData(legend);
                }
            }
        }
    }


    private void createModel(ECharts echarts, List<Object[]> model) {
        Map<String, Object> gauge = null;
        if (echarts.getOption().getSeries() instanceof Map)
            gauge = (Map<String, Object>) echarts.getOption().getSeries();
        else if (echarts.getOption().getSeries() instanceof List)
            gauge = (Map<String, Object>) ((List) echarts.getOption().getSeries()).get(0);
        List<Object> data = new ArrayList<>();
        List<Object> legend = new ArrayList<>();
        for (Object[] objs : model) {
            Map<String, Object> row = new HashMap<>();
            row.put("name", objs[0]);
            row.put("value", objs[1]);
            data.add(row);
            legend.add(objs[0]);
        }
        gauge.put("data", data);
        if (echarts.getOption().getLegend() != null)
            echarts.getOption().getLegend().setData(legend);
    }
}

