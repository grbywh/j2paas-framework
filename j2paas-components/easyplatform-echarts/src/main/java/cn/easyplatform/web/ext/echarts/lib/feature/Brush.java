/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.feature;

import org.zkoss.util.resource.Labels;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Brush extends Feature {

    private Object type;

    public Brush() {
        this.show(true);
        Map title = new HashMap<String, String>();
        title.put("rect", Labels.getLabel("echarts.title.rect"));
        title.put("polygon", Labels.getLabel("echarts.title.polygon"));
        title.put("lineX", Labels.getLabel("echarts.title.lineX"));
        title.put("lineY", Labels.getLabel("echarts.title.lineY"));
        title.put("keep", Labels.getLabel("echarts.title.keep"));
        title.put("clear", Labels.getLabel("echarts.title.clear"));
        this.title(title);
    }

    public Object type() {
        return this.type;
    }

    public Brush type(Object value) {
        if (value instanceof String)
            this.type = ((String) value).split(",");
        else
            this.type = value;
        return this;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

}
