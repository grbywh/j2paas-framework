/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.support.MarkArea;
import cn.easyplatform.web.ext.echarts.lib.support.MarkLine;
import cn.easyplatform.web.ext.echarts.lib.support.MarkPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Boxplot implements Serializable {
    private String type = "boxplot";
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private String name;
    private Boolean legendHoverLink;
    private Boolean hoverAnimation;
    private Object layout;
    private Object boxWidth;
    private ItemStyle itemStyle;
    private List<Object> data;
    private MarkPoint markPoint;
    private MarkLine markLine;
    private MarkArea markArea;
    private Integer zlevel;
    private Integer z;
    private Integer animationDuration;
    private Object animationEasing;
    private Object animationDelay;
    private Tooltip tooltip;

    private Emphasis emphasis;
    /**
     * 使用 dimensions 定义 series.data 或者 dataset.source 的每个维度的信息。
     */
    private Object dimensions;
    /**
     * 可以定义 data 的哪个维度被编码成什么
     */
    private Object encode;

    public Object encode() {
        return this.encode;
    }

    public Boxplot encode(Object encode) {
        this.encode = encode;
        return this;
    }

    public Object dimensions() {
        return this.dimensions;
    }

    public Boxplot dimensions(Object dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Boxplot emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return tooltip;
    }

    public Boxplot tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public String type() {
        return type;
    }

    public Boxplot type(String type) {
        this.type = type;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Boxplot coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Boxplot xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Boxplot yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public String name() {
        return name;
    }

    public Boxplot name(String name) {
        this.name = name;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Boxplot legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Object hoverAnimation() {
        return hoverAnimation;
    }

    public Boxplot hoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public Object layout() {
        return layout;
    }

    public Boxplot layout(Object layout) {
        this.layout = layout;
        return this;
    }

    public Object boxWidth() {
        return boxWidth;
    }

    public Boxplot boxWidth(Object value) {
        if (value instanceof String) {
            this.boxWidth = value.toString().split(",");
        } else
            this.boxWidth = value;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Boxplot itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Boxplot data(Object values) {
        if (data == null)
            data = new ArrayList<Object>();
        this.data.add(values);
        return this;
    }

    public MarkPoint markPoint() {
        if (markPoint == null)
            markPoint = new MarkPoint();
        return markPoint;
    }

    public Boxplot markPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
        return this;
    }

    public MarkLine markLine() {
        if (markLine == null)
            markLine = new MarkLine();
        return markLine;
    }

    public Boxplot markLine(MarkLine markLine) {
        this.markLine = markLine;
        return this;
    }

    public MarkArea markArea() {
        if (markArea == null)
            markArea = new MarkArea();
        return markArea;
    }

    public Boxplot markArea(MarkArea markArea) {
        this.markArea = markArea;
        return this;
    }

    public Boxplot zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public Boxplot z(Integer z) {
        this.z = z;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public Integer animationDuration() {
        return animationDuration;
    }

    public Boxplot animationDuration(Integer animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    public Object animationEasing() {
        return animationEasing;
    }

    public Boxplot animationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
        return this;
    }

    public Object animationDelay() {
        return animationDelay;
    }

    public Boxplot animationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Boolean getHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }

    public Object getLayout() {
        return layout;
    }

    public void setLayout(Object layout) {
        this.layout = layout;
    }

    public Object getBoxWidth() {
        return boxWidth;
    }

    public void setBoxWidth(Object boxWidth) {
        this.boxWidth = boxWidth;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public List<Object> getData() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public MarkPoint getMarkPoint() {
        return markPoint;
    }

    public void setMarkPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
    }

    public MarkLine getMarkLine() {
        return markLine;
    }

    public void setMarkLine(MarkLine markLine) {
        this.markLine = markLine;
    }

    public MarkArea getMarkArea() {
        return markArea;
    }

    public void setMarkArea(MarkArea markArea) {
        this.markArea = markArea;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Integer getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(Integer animationDuration) {
        this.animationDuration = animationDuration;
    }

    public Object getAnimationEasing() {
        return animationEasing;
    }

    public void setAnimationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
    }

    public Object getAnimationDelay() {
        return animationDelay;
    }

    public void setAnimationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Object getDimensions() {
        return dimensions;
    }

    public void setDimensions(Object dimensions) {
        this.dimensions = dimensions;
    }

    public Object getEncode() {
        return encode;
    }

    public void setEncode(Object encode) {
        this.encode = encode;
    }
}