/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;
import java.util.List;

public class ViewControl implements Serializable {
    /**
     * 投影方式，默认为透视投影'perspective'，也支持设置为正交投影'orthographic'。
     */
    private String perspective;
    /**
     * 是否开启视角绕物体的自动旋转查看。
     */
    private boolean autoRotate;
    /**
     * 物体自转的方向。默认是 'cw' 也就是从上往下看是顺时针方向，也可以取 'ccw'，既从上往下看为逆时针方向。
     */
    private String autoRotateDirection;
    /**
     * 物体自转的速度。单位为角度 / 秒，默认为10 ，也就是36秒转一圈。
     */
    private int autoRotateSpeed;
    /**
     * 在鼠标静止操作后恢复自动旋转的时间间隔。在开启 autoRotate 后有效。
     */
    private int autoRotateAfterStill;
    /**
     * 鼠标进行旋转，缩放等操作时的迟滞因子，在大于 0 的时候鼠标在停止操作后，视角仍会因为一定的惯性继续运动（旋转和缩放）。
     */
    private int damping;
    /**
     * 旋转操作的灵敏度，值越大越灵敏。支持使用数组分别设置横向和纵向的旋转灵敏度。
     */
    private int rotateSensitivity;
    /**
     * 缩放操作的灵敏度，值越大越灵敏。默认为1。
     */
    private int zoomSensitivity;
    /**
     * 平移操作的灵敏度，值越大越灵敏。支持使用数组分别设置横向和纵向的平移灵敏度
     */
    private int panSensitivity;
    /**
     * 平移操作使用的鼠标按键，支持：
     *
     * 'left' 鼠标左键（默认）
     *
     * 'middle' 鼠标中键
     *
     * 'right' 鼠标右键
     *
     * 注意：如果设置为鼠标右键则会阻止默认的右键菜单。
     */
    private String panMouseButton;
    /**
     * 旋转操作使用的鼠标按键，支持：
     *
     * 'left' 鼠标左键
     *
     * 'middle' 鼠标中键（默认）
     *
     * 'right' 鼠标右键
     *
     * 注意：如果设置为鼠标右键则会阻止默认的右键菜单。
     */
    private String rotateMouseButton;
    /**
     * 默认视角距离主体的距离，对于 globe 来说是距离地球表面的距离，对于 grid3D 和 geo3D 等其它组件来说是距离中心原点的距离。
     * 在 projection 为'perspective'的时候有效。
     */
    private int distance;
    /**
     * 视角通过鼠标控制能拉近到主体的最小距离。在 projection 为'perspective'的时候有效。
     */
    private int minDistance;
    /**
     * 视角通过鼠标控制能拉远到主体的最大距离。在 projection 为'perspective'的时候有效。
     */
    private int maxDistance;
    /**
     * 正交投影的大小。在 projection 为'orthographic'的时候有效。
     */
    private int orthographicSize;
    /**
     * 正交投影缩放的最大值。在 projection 为'orthographic'的时候有效。
     */
    private int  maxOrthographicSize;
    /**
     * 正交投影缩放的最小值。在 projection 为'orthographic'的时候有效。
     */
    private int minOrthographicSize;
    /**
     * 视角绕 x 轴，即上下旋转的角度。配合 beta 可以控制视角的方向。
     */
    private int alpha;
    /**
     * 视角绕 y 轴，即左右旋转的角度。
     */
    private int beta;
    /**
     * 视角中心点，旋转也会围绕这个中心点旋转，默认为[0,0,0]。
     */
    private List<Integer> center;
    /**
     * 上下旋转的最小 alpha 值。即视角能旋转到达最上面的角度。
     */
    private int minAlpha;
    /**
     * 上下旋转的最大 alpha 值。即视角能旋转到达最下面的角度。
     */
    private int maxAlpha;
    /**
     * 左右旋转的最小 beta 值。即视角能旋转到达最左的角度。
     */
    private Object minBeta;
    /**
     * 左右旋转的最大 beta 值。即视角能旋转到达最右的角度。
     */
    private Object maxBeta;
    /**
     * 是否开启动画。
     */
    private boolean animation;
    /**
     * 过渡动画的时长。
     */
    private int animationDurationUpdate;
    /**
     * 过渡动画的缓动效果。
     */
    private String animationEasingUpdate;

    public String perspective() {
        return this.perspective;
    }
    public ViewControl perspective(String perspective) {
        this.perspective = perspective;
        return this;
    }

    public Boolean autoRotate() {
        return this.autoRotate;
    }
    public ViewControl autoRotate(Boolean autoRotate) {
        this.autoRotate = autoRotate;
        return this;
    }

    public String autoRotateDirection() {
        return this.autoRotateDirection;
    }
    public ViewControl autoRotateDirection(String autoRotateDirection) {
        this.autoRotateDirection = autoRotateDirection;
        return this;
    }

    public Integer autoRotateSpeed() {
        return this.autoRotateSpeed;
    }
    public ViewControl autoRotateSpeed(Integer autoRotateSpeed) {
        this.autoRotateSpeed = autoRotateSpeed;
        return this;
    }

    public Integer autoRotateAfterStill() {
        return this.autoRotateAfterStill;
    }
    public ViewControl autoRotateAfterStill(Integer autoRotateAfterStill) {
        this.autoRotateAfterStill = autoRotateAfterStill;
        return this;
    }

    public Integer damping() {
        return this.damping;
    }
    public ViewControl damping(Integer damping) {
        this.damping = damping;
        return this;
    }

    public Integer rotateSensitivity() {
        return this.rotateSensitivity;
    }
    public ViewControl rotateSensitivity(Integer rotateSensitivity) {
        this.rotateSensitivity = rotateSensitivity;
        return this;
    }

    public Integer zoomSensitivity() {
        return this.zoomSensitivity;
    }
    public ViewControl zoomSensitivity(Integer zoomSensitivity) {
        this.zoomSensitivity = zoomSensitivity;
        return this;
    }

    public Integer panSensitivity() {
        return this.panSensitivity;
    }
    public ViewControl panSensitivity(Integer panSensitivity) {
        this.panSensitivity = panSensitivity;
        return this;
    }

    public String panMouseButton() {
        return this.panMouseButton;
    }
    public ViewControl panMouseButton(String panMouseButton) {
        this.panMouseButton = panMouseButton;
        return this;
    }

    public String rotateMouseButton() {
        return this.rotateMouseButton;
    }
    public ViewControl rotateMouseButton(String rotateMouseButton) {
        this.rotateMouseButton = rotateMouseButton;
        return this;
    }

    public Integer distance() {
        return this.distance;
    }
    public ViewControl distance(Integer distance) {
        this.distance = distance;
        return this;
    }

    public Integer minDistance() {
        return this.minDistance;
    }
    public ViewControl minDistance(Integer minDistance) {
        this.minDistance = minDistance;
        return this;
    }

    public Integer maxDistance() {
        return this.maxDistance;
    }
    public ViewControl maxDistance(Integer maxDistance) {
        this.maxDistance = maxDistance;
        return this;
    }

    public Integer orthographicSize() {
        return this.orthographicSize;
    }
    public ViewControl orthographicSize(Integer orthographicSize) {
        this.orthographicSize = orthographicSize;
        return this;
    }

    public Integer maxOrthographicSize() {
        return this.maxOrthographicSize;
    }
    public ViewControl maxOrthographicSize(Integer maxOrthographicSize) {
        this.maxOrthographicSize = maxOrthographicSize;
        return this;
    }

    public Integer minOrthographicSize() {
        return this.minOrthographicSize;
    }
    public ViewControl minOrthographicSize(Integer minOrthographicSize) {
        this.minOrthographicSize = minOrthographicSize;
        return this;
    }

    public Integer alpha() {
        return this.alpha;
    }
    public ViewControl alpha(Integer alpha) {
        this.alpha = alpha;
        return this;
    }

    public Integer beta() {
        return this.beta;
    }
    public ViewControl beta(Integer beta) {
        this.beta = beta;
        return this;
    }

    public List<Integer> center() {
        return this.center;
    }
    public ViewControl center(List<Integer> center) {
        this.center = center;
        return this;
    }

    public Integer minAlpha() {
        return this.minAlpha;
    }
    public ViewControl minAlpha(Integer minAlpha) {
        this.minAlpha = minAlpha;
        return this;
    }

    public Integer maxAlpha() {
        return this.maxAlpha;
    }
    public ViewControl maxAlpha(Integer maxAlpha) {
        this.maxAlpha = maxAlpha;
        return this;
    }

    public Object minBeta() {
        return this.minBeta;
    }
    public ViewControl minBeta(Object minBeta) {
        this.minBeta = minBeta;
        return this;
    }

    public Object maxBeta() {
        return this.maxBeta;
    }
    public ViewControl maxBeta(Object maxBeta) {
        this.maxBeta = maxBeta;
        return this;
    }

    public Boolean animation() {
        return this.animation;
    }
    public ViewControl animation(Boolean animation) {
        this.animation = animation;
        return this;
    }

    public Integer animationDurationUpdate() {
        return this.animationDurationUpdate;
    }
    public ViewControl animationDurationUpdate(Integer animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;
        return this;
    }

    public String animationEasingUpdate() {
        return this.animationEasingUpdate;
    }
    public ViewControl animationEasingUpdate(String animationEasingUpdate) {
        this.animationEasingUpdate = animationEasingUpdate;
        return this;
    }

    public String getPerspective() {
        return perspective;
    }

    public void setPerspective(String perspective) {
        this.perspective = perspective;
    }

    public boolean isAutoRotate() {
        return autoRotate;
    }

    public void setAutoRotate(boolean autoRotate) {
        this.autoRotate = autoRotate;
    }

    public String getAutoRotateDirection() {
        return autoRotateDirection;
    }

    public void setAutoRotateDirection(String autoRotateDirection) {
        this.autoRotateDirection = autoRotateDirection;
    }

    public int getAutoRotateSpeed() {
        return autoRotateSpeed;
    }

    public void setAutoRotateSpeed(int autoRotateSpeed) {
        this.autoRotateSpeed = autoRotateSpeed;
    }

    public int getAutoRotateAfterStill() {
        return autoRotateAfterStill;
    }

    public void setAutoRotateAfterStill(int autoRotateAfterStill) {
        this.autoRotateAfterStill = autoRotateAfterStill;
    }

    public int getDamping() {
        return damping;
    }

    public void setDamping(int damping) {
        this.damping = damping;
    }

    public int getRotateSensitivity() {
        return rotateSensitivity;
    }

    public void setRotateSensitivity(int rotateSensitivity) {
        this.rotateSensitivity = rotateSensitivity;
    }

    public int getZoomSensitivity() {
        return zoomSensitivity;
    }

    public void setZoomSensitivity(int zoomSensitivity) {
        this.zoomSensitivity = zoomSensitivity;
    }

    public int getPanSensitivity() {
        return panSensitivity;
    }

    public void setPanSensitivity(int panSensitivity) {
        this.panSensitivity = panSensitivity;
    }

    public String getPanMouseButton() {
        return panMouseButton;
    }

    public void setPanMouseButton(String panMouseButton) {
        this.panMouseButton = panMouseButton;
    }

    public String getRotateMouseButton() {
        return rotateMouseButton;
    }

    public void setRotateMouseButton(String rotateMouseButton) {
        this.rotateMouseButton = rotateMouseButton;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public int getOrthographicSize() {
        return orthographicSize;
    }

    public void setOrthographicSize(int orthographicSize) {
        this.orthographicSize = orthographicSize;
    }

    public int getMaxOrthographicSize() {
        return maxOrthographicSize;
    }

    public void setMaxOrthographicSize(int maxOrthographicSize) {
        this.maxOrthographicSize = maxOrthographicSize;
    }

    public int getMinOrthographicSize() {
        return minOrthographicSize;
    }

    public void setMinOrthographicSize(int minOrthographicSize) {
        this.minOrthographicSize = minOrthographicSize;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getBeta() {
        return beta;
    }

    public void setBeta(int beta) {
        this.beta = beta;
    }

    public List<Integer> getCenter() {
        return center;
    }

    public void setCenter(List<Integer> center) {
        this.center = center;
    }

    public int getMinAlpha() {
        return minAlpha;
    }

    public void setMinAlpha(int minAlpha) {
        this.minAlpha = minAlpha;
    }

    public int getMaxAlpha() {
        return maxAlpha;
    }

    public void setMaxAlpha(int maxAlpha) {
        this.maxAlpha = maxAlpha;
    }

    public Object getMinBeta() {
        return minBeta;
    }

    public void setMinBeta(Object minBeta) {
        this.minBeta = minBeta;
    }

    public Object getMaxBeta() {
        return maxBeta;
    }

    public void setMaxBeta(Object maxBeta) {
        this.maxBeta = maxBeta;
    }

    public boolean isAnimation() {
        return animation;
    }

    public void setAnimation(boolean animation) {
        this.animation = animation;
    }

    public int getAnimationDurationUpdate() {
        return animationDurationUpdate;
    }

    public void setAnimationDurationUpdate(int animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;
    }

    public String getAnimationEasingUpdate() {
        return animationEasingUpdate;
    }

    public void setAnimationEasingUpdate(String animationEasingUpdate) {
        this.animationEasingUpdate = animationEasingUpdate;
    }
}
