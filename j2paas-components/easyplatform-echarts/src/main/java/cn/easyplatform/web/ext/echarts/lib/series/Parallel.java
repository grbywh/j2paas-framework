/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Parallel extends Series {
    private String coordinateSystem;
    private LineStyle lineStyle;
    private Double inactiveOpacity;
    private Object activeOpacity;
    private Boolean realtime;
    private Boolean smooth;

    /**
     * 使用的平行坐标系的 index，在单个图表实例中存在多个平行坐标系的时候有用。
     */
    private Integer parallelIndex;
    /**
     * 渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置
     */
    private Object progressive;
    /**
     * 启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
     */
    private Object progressiveThreshold;
    /**
     * 分片的方式。可选值：
     * <p>
     * 'sequential': 按照数据的顺序分片。缺点是渲染过程不自然。
     * 'mod': 取模分片，即每个片段中的点会遍布于整个数据，从而能够视觉上均匀得渲染。
     */
    private String progressiveChunkMode;

    public Parallel() {
        this.type="parallel";
    }

    public String progressiveChunkMode() {
        return progressiveChunkMode;
    }

    public Parallel progressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
        return this;
    }

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Parallel progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return progressive;
    }

    public Parallel progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }

    public Integer parallelIndex() {
        return parallelIndex;
    }

    public Parallel parallelIndex(Integer parallelIndex) {
        this.parallelIndex = parallelIndex;
        return this;
    }

    public Boolean smooth() {
        return smooth;
    }

    public Parallel smooth(Boolean smooth) {
        this.smooth = smooth;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Parallel coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return this.lineStyle;
    }

    public Parallel lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Double inactiveOpacity() {
        return inactiveOpacity;
    }

    public Parallel inactiveOpacity(Double inactiveOpacity) {
        this.inactiveOpacity = inactiveOpacity;
        return this;
    }

    public Object activeOpacity() {
        return activeOpacity;
    }

    public Parallel activeOpacity(Object activeOpacity) {
        this.activeOpacity = activeOpacity;
        return this;
    }

    public Boolean realtime() {
        return realtime;
    }

    public Parallel realtime(Boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Double getInactiveOpacity() {
        return inactiveOpacity;
    }

    public void setInactiveOpacity(Double inactiveOpacity) {
        this.inactiveOpacity = inactiveOpacity;
    }

    public Object getActiveOpacity() {
        return activeOpacity;
    }

    public void setActiveOpacity(Object activeOpacity) {
        this.activeOpacity = activeOpacity;
    }

    public Boolean getRealtime() {
        return realtime;
    }

    public void setRealtime(Boolean realtime) {
        this.realtime = realtime;
    }
}
