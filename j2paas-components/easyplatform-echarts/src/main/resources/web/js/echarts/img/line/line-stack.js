{
    title: {
        text: '折线图堆叠'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: 'data'
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: '邮件营销',
            type: 'line',
            stack: '总量',
            data: 'data'
        },
        {
            name: '联盟广告',
            type: 'line',
            stack: '总量',
            data: 'data'
        },
        {
            name: '视频广告',
            type: 'line',
            stack: '总量',
            data: 'data'
        },
        {
            name: '直接访问',
            type: 'line',
            stack: '总量',
            data: 'data'
        },
        {
            name: '搜索引擎',
            type: 'line',
            stack: '总量',
            data: 'data'
        }
    ]
}