zk.afterLoad('zul.inp', function () {
    var _input = {};
    zk.override(zul.inp.NumberInputWidget.prototype, _input, {
        getAllowedKeys_: function () {
            return _input.getAllowedKeys_.apply(this, arguments) + 'htmbHTMB';
        },
        doKeyUp_: function () {
            var inp = this.getInputNode(), val = inp.value;
            inp.value = val.replace(/h/g, "00").replace(/H/g, "00").replace(/t/g, "000").replace(/T/g, "000").replace(/m/g, "000000").replace(/M/g, "000000").replace(/b/g, "000000000").replace(/B/g, "000000000");
            _input.doKeyUp_.apply(this, arguments);
        }
    });
    var _bandbox = {}
    _bandboxMolds = {};
    zk.override(zul.inp.Bandbox.molds, _bandboxMolds, {
        'wb': zul.inp.Bandbox.molds['default']
    });
    zk.override(zul.inp.Bandbox.prototype, _bandbox, {
        $init: function () {
            _bandbox.$init.apply(this, arguments);
            this._separatorCode = ',';
        },
        setSeparator: function (v) {
            this._separatorCode = v;
        },
        setValue: function (value, fromServer) {
            _bandbox.setValue.apply(this, arguments);
            if (this.$n() && (this.getMold() === 'pp' || this.getMold() === 'dd')) {
                this._value = value;
                jq(this.$n('real')).find('.z-chosenbox-item').remove();
                this._drawItem();
            }
        },
        _drawItem: function () {
            if (this._value) {
                var items = this._value.split(this._separatorCode);
                for (var i = 0; i < items.length; i++)
                    this._createItem(items[i]);
            }
        },
        _createItem: function (value) {
            var span = document.createElement("span")
                , content = document.createElement("div")
                , delbtn = document.createElement("div")
                , icon = document.createElement("i")
                , wgt = this;
            span.className = 'z-chosenbox-item';
            content.innerHTML = value;
            content.className = "z-chosenbox-item-content";
            delbtn.className = 'z-chosenbox-button z-chosenbox-delete';
            icon.className = 'z-chosenbox-icon z-icon-times';
            span.appendChild(content);
            span.appendChild(delbtn);
            delbtn.appendChild(icon);
            jq(delbtn).bind("click", function () {
                if (!wgt.isDisabled()) {
                    wgt.fire("onChange", {value: jq(span).index()}, {sendAhead: true});
                    jq(span).remove();
                }
            });
            this.$n('real').appendChild(span);
        },
        getIconSclass: function () {
            var sc = _bandbox.getIconSclass.apply(this, arguments);
            var mold = this.getMold();
            if (mold === 'dd' || mold === 'pp')
                return 'z-icon-flash';
            return sc;
        },
        enterPressed_: function () {
            if (this.getMold() == 'wb')
                this.fire('onCtrlKey');
            else
                _bandbox.enterPressed_.apply(this, arguments);
        },
        escPressed_: function () {
            if (this.getMold() != 'wb')
                _bandbox.escPressed_.apply(this, arguments);
        },
        bind_: function () {
            _bandbox.bind_.apply(this, arguments);
            if (this.getMold() === 'pp' || this.getMold() === 'dd') {
                jq(this.$n()).css('display', 'inline-flex');
                this._drawItem();
            }
        },
        redraw: function (out) {
            var mold = this.getMold();
            if (mold === 'pp' || mold === 'dd') {
                var uuid = this.uuid,
                    isButtonVisible = this._buttonVisible;
                out.push('<span', this.domAttrs_({text: true}), '><i id="',
                    uuid, '-real" class="z-chosenbox');
                if (!isButtonVisible)
                    out.push(' ', this.$s('rightedge'));
                out.push('"', 'style="white-space:nowrap;border-right:0;width:100%"', '/><a id="', uuid, '-btn" class="', this.$s('button'));
                if (!isButtonVisible)
                    out.push(' ', this.$s('disabled'));
                out.push('"><i class="', this.getIconSclass(), '" style="font-size:14px', '"></i></a>');
                if (mold === 'dd')
                    this.redrawpp_(out);
                out.push('</span>');
            } else
                _bandbox.redraw.apply(this, arguments);
        },
        unbind_: function () {
            if (this.getMold() === 'pp' || this.getMold() === 'dd')
                jq(this.$n('real')).remove();
            _bandbox.unbind_.apply(this, arguments);
        }
    });
});