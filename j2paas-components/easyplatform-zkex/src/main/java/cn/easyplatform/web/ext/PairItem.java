/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PairItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Object value;

    private String label;

    /**
     * @param value
     */
    public PairItem(Object value) {
        this.value = value;
        this.label = value == null ? "" : value.toString();
    }

    /**
     * @param value
     * @param label
     */
    public PairItem(Object value, String label) {
        this.value = value;
        this.label = label;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int hashCode() {
        return value == null ? "".hashCode() : value.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof PairItem))
            return false;
        String v1 = ((PairItem) obj).value == null ? ""
                : ((PairItem) obj).value.toString();
        String v2 = value == null ? "" : value.toString();
        return v1.equals(v2);
    }

    @Override
    public String toString() {
        return label;
    }

}
