/* Copyright 2013-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snaker.engine.impl;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.snaker.engine.INoGenerator;
import org.snaker.engine.model.ProcessModel;

import java.util.Random;

/**
 * 默认的流程实例编号生成器 编号生成规则为:yyyyMMdd-HH:mm:ss-SSS-random
 * 
 * @author yuqs
 * @version 1.0
 */
public class DefaultNoGenerator implements INoGenerator {
	public String generate(ProcessModel model) {
		String time = DateFormatUtils.format(System.currentTimeMillis(),
				"yyyyMMdd-HH:mm:ss-SSS");
		Random random = new Random();
		return time + "-" + random.nextInt(1000);
	}
}
