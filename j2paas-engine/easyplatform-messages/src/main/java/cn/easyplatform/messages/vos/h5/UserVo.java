/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.h5;

import java.io.Serializable;

import cn.easyplatform.utils.ColorUtil;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UserVo extends IdNameVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1873239715091913985L;

	// 未登陆
	public final static int STATUS_INVALID = 0;
	// 已登陆，但不支持MSN
	public final static int STATUS_UNLINK = 1;
	// 已登陆且支持MSN
	public final static int STATUS_LINK = 2;

	private int status;

	private String color;
	
	private transient boolean inGroup;
	
	/**
	 * @param id
	 * @param name
	 */
	public UserVo(String id, String name) {
		super(id, name);
		status = STATUS_INVALID;
		this.color = ColorUtil.random();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getColor() {
		return color;
	}

	public boolean isInGroup() {
		return inGroup;
	}

	public void setInGroup(boolean inGroup) {
		this.inGroup = inGroup;
	}
	
}
