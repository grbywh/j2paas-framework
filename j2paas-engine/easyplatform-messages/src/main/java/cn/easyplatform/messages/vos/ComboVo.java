/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ComboVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6699481866951645545L;

	private String listId;
	
	private String name;
	
	private String query;
	
	private Object[] params;

	/**
	 * @param listId
	 * @param name
	 * @param query
	 * @param params
	 */
	public ComboVo(String listId, String name, String query, Object[] params) {
		super();
		this.listId = listId;
		this.name = name;
		this.query = query;
		this.params = params;
	}

	public String getListId() {
		return listId;
	}

	public String getQuery() {
		return query;
	}

	public Object[] getParams() {
		return params;
	}

	public String getName() {
		return name;
	}
	
	
}
