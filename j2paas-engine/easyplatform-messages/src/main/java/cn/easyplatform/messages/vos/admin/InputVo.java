/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import cn.easyplatform.type.FieldType;

import java.io.Serializable;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/4/28 16:02
 * @Modified By:
 */
public class InputVo implements Serializable {
    private String name;
    private String desp;
    private FieldType type;
    private int length;
    private int decimal;
    private boolean required;

    public InputVo(String name, String desp, FieldType type, int length, int decimal, boolean required) {
        this.name = name;
        this.desp = desp;
        this.type = type;
        this.length = length;
        this.decimal = decimal;
        this.required = required;
    }

    public String getDesp() {
        return desp;
    }

    public String getName() {
        return name;
    }

    public FieldType getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public int getDecimal() {
        return decimal;
    }

    public boolean isRequired() {
        return required;
    }
}
