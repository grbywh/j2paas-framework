/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.extension;

import java.util.Map;
import java.util.Set;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ApplicationContext {

    /**
     * 获取项目信息
     *
     * @return
     */
    ProjectContext getProjectContext(String projectId);

    /**
     * @param name
     * @return
     */
    String getConfig(String name);

    /**
     * 配置项名称
     *
     * @return
     */
    Set<String> keys();

    /**
     * 获取平台配置
     *
     * @param name
     * @return
     */
    Map<String, String> getApplicationConfig(String name);

    /**
     * 获取平台指定的配置项
     *
     * @param group
     * @param name
     * @return
     */
    String getApplicationConfigValue(String group, String name);
}
