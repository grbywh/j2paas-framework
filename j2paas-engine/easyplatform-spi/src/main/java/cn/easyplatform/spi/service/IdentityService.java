/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IdentityService {

    /**
     * @return
     */
    IResponseMessage<?> getAppEnv(SimpleRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> login(LoginRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> selectOrg(SimpleRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> logout(SimpleRequestMessage req);

    /**
     * 确认是否继续登陆
     *
     * @param req
     * @return
     */
    IResponseMessage<?> confirm(SimpleRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> apiInit(ApiInitRequestMessage req);

    /**
     * 修改密码
     *
     * @param req
     * @return
     */
    IResponseMessage<?> changePassword(ChangePasswordRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> check(CheckRequestMessage req);

    /**
     * 回调
     *
     * @param req
     * @return
     */
    IResponseMessage<?> callback(SimpleRequestMessage req);
}
