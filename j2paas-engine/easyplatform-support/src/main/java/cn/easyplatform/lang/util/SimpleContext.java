/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 可以用来存储无序名值对
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public class SimpleContext extends AbstractContext {

	private Map<String, Object> map;

	public SimpleContext() {
		this(new HashMap<String, Object>());
	}

	public SimpleContext(Map<String, Object> map) {
		this.map = map;
	}

	public int size() {
		return map.size();
	}

	public Context set(String name, Object value) {
		map.put(name, value);
		return this;
	}

	public Set<String> keys() {
		return map.keySet();
	}

	public boolean has(String key) {
		return map.containsKey(key);
	}

	public Map<String, Object> getInnerMap() {
		return map;
	}

	public Context clear() {
		this.map.clear();
		return this;
	}

	public Object get(String name) {
		return map.get(name);
	}

	public SimpleContext clone() {
		SimpleContext context = new SimpleContext();
		context.map.putAll(this.map);
		return context;
	}

	@Override
	public Object remove(String name) {
		return map.remove(name);
	}
}
