/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.eject;

import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;

import java.lang.reflect.Method;

public class EjectBySimpleEL implements Ejecting {

	private String by;

	private Method method;

	public EjectBySimpleEL(String by) {
		if (Strings.isBlank(by))
			throw new IllegalArgumentException("MUST NOT Null/Blank");
		if (by.indexOf('#') > 0) {
			try {
				method = Class.forName(by.substring(0, by.indexOf('#')))
						.getMethod(by.substring(by.indexOf('#') + 1),
								Object.class);
			} catch (Throwable e) {
				throw Lang.wrapThrow(e);
			}
		}
		this.by = by;
	}

	public Object eject(Object obj) {
		try {
			if (method != null)
				return method.invoke(null, obj);
			if (obj == null)
				return null;
			return obj.getClass().getMethod(by).invoke(obj);
		} catch (Throwable e) {
			throw Lang.wrapThrow(e);
		}
	}

}
