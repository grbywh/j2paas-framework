/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.helper;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PropertyAdapter extends
		XmlAdapter<Property[], Map<String, String>> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Map<String, String> unmarshal(Property[] v) throws Exception {
		Map<String, String> props = new HashMap<String, String>();
		for (Property prop : v) {
			props.put(prop.getName(), prop.getValue());
		}
		return props;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Property[] marshal(Map<String, String> v) throws Exception {
		if (v == null)
			return new Property[0];
		Property[] props = new Property[v.size()];
		int index = 0;
		for (Map.Entry<String, String> entry : v.entrySet()) {
			Property prop = new Property();
			prop.setName(entry.getKey());
			prop.setValue(entry.getValue());
			props[index] = prop;
			index++;
		}
		return props;
	}

}
