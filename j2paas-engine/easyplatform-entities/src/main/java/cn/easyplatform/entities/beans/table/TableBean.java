/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.table;

import cn.easyplatform.entities.BaseEntity;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"fields", "key", "indexes", "foreignKeys", "view", "cacheable"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "table")
public class TableBean extends BaseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2864336272611289633L;

    @XmlAttribute
    private boolean autoKey;// 键值自动生成

    /**
     * 2019-08-07，在不影响旧的规格，增加新的属性,键值增长是否使用表栏位，当autoKey为true时使用，
     */
    @XmlAttribute
    private boolean byDb;

    @XmlAttribute
    private boolean cacheable;//是否启用缓存

    @XmlElement
    private String view;// 视图

    @XmlElementWrapper(name = "fields")
    @XmlElement(name = "field")
    private List<TableField> fields;

    @XmlElementWrapper(name = "key")
    @XmlElement(name = "field", required = true)
    private List<String> key;

    @XmlElementWrapper(name = "indexes")
    @XmlElement(name = "index")
    private List<TableIndex> indexes;

    @XmlElementWrapper(name = "foreign-keys")
    @XmlElement(name = "foreign-key")
    private List<TableFk> foreignKeys;

    /**
     * @return the view
     */
    public String getView() {
        return view;
    }

    /**
     * @param view the view to set
     */
    public void setView(String view) {
        this.view = view;
    }

    /**
     * @return the fields
     */
    public List<TableField> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<TableField> fields) {
        this.fields = fields;
    }

    /**
     * @return the key
     */
    public List<String> getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(List<String> key) {
        this.key = key;
    }

    /**
     * @return the indexes
     */
    public List<TableIndex> getIndexes() {
        return indexes;
    }

    /**
     * @param indexes the indexes to set
     */
    public void setIndexes(List<TableIndex> indexes) {
        this.indexes = indexes;
    }

    /**
     * @return the foreignKeys
     */
    public List<TableFk> getForeignKeys() {
        return foreignKeys;
    }

    /**
     * @param foreignKeys the foreignKeys to set
     */
    public void setForeignKeys(List<TableFk> foreignKeys) {
        this.foreignKeys = foreignKeys;
    }

    @Override
    public String toString() {
        return getId();
    }

    public boolean isAutoKey() {
        return autoKey;
    }

    public void setAutoKey(boolean autoKey) {
        this.autoKey = autoKey;
    }

    public boolean isCacheable() {
        return cacheable;
    }

    public void setCacheable(boolean cacheable) {
        this.cacheable = cacheable;
    }

    public boolean isByDb() {
        return byDb;
    }

    public void setByDb(boolean byDb) {
        this.byDb = byDb;
    }
}
