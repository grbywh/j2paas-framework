/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services;

import cn.easyplatform.cfg.*;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dao.IdentityDao;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.entities.beans.project.DeviceMapBean;
import cn.easyplatform.entities.beans.project.PortletBean;
import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.services.project.ProjectConfig;
import cn.easyplatform.spi.extension.ApplicationService;
import org.apache.shiro.cache.Cache;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IProjectService extends ApplicationService {

    /**
     * 支持的语言
     *
     * @return
     */
    List<String> getLanguages();

    /**
     * 默认地区
     *
     * @return
     */
    Locale getLocale();

    /**
     * 获取对应的portlet页面
     *
     * @param portlet
     * @param deviceType
     * @return
     */
    DeviceMapBean getDeviceMap(String portlet, String deviceType);

    /**
     * 获取对应的portlet页面
     *
     * @param portlet
     * @return
     */
    PortletBean getPortlet(String portlet);

    /**
     * 检查用户
     *
     * @param user
     * @param check
     */
    void validate(UserDo user, boolean check);

    /**
     * 项目工作路径
     *
     * @return
     */
    String getWorkspace();

    /**
     * 业务dao
     *
     * @return
     */
    BizDao getBizDao();

    /**
     * 业务dao
     *
     * @param dsId
     * @return
     */
    BizDao getBizDao(String dsId);

    /**
     * 用户验证dao
     *
     * @return
     */
    IdentityDao getIdentityDao();

    /**
     * 工作路径
     *
     * @return
     */
    String getWorkPath();

    /**
     * 资源管理接口
     *
     * @return
     */
    IResourceHandler getResourceHandler();

    /**
     * 任务调度管理接口
     *
     * @return
     */
    IScheduleHandler getScheduleHandler();

    /**
     * 工作流管理接口
     *
     * @param cc
     * @return
     */
    BpmEngineContext getBpmEngine(CommandContext cc);

    /**
     * 节假日管理接口
     *
     * @param cc
     * @return
     */
    BusinessDateHandler getBusinessDateHandler(CommandContext cc);

    /**
     * 记录锁管理接口
     *
     * @param cc
     * @return
     */
    RowLockProvider getRowLockProvider(CommandContext cc);

    /**
     * 事务接口
     *
     * @param cc
     * @return
     */
    TransactionContext getTransactionContext(CommandContext cc);

    /**
     * 全局id生成器
     *
     * @return
     */
    IdGenerator getIdGenerator();

    /**
     * 用户会话管理接口
     *
     * @return
     */
    ISessionManager getSessionManager();

    /**
     * 项目配置接口
     *
     * @return
     */
    ProjectConfig getConfig();

    /**
     * 实体参数管理接口
     *
     * @return
     */
    IEntityHandler getEntityHandler();

    /**
     * 刷新服务
     *
     * @param type
     */
    void refresh(String type);

    /**
     * 获取自定义对象
     *
     * @param name
     * @return
     */
    Object getCommand(String name);

    /**
     * 获取项目参数
     *
     * @return
     */
    ProjectBean getEntity();

    /**
     * 重设项目参数
     *
     * @param obj
     */
    void save(Object obj);


    /**
     * 增加服务
     *
     * @param service
     */
    void addService(ApplicationService service);

    /**
     * 删除服务
     *
     * @param id
     */
    void removeService(String id);

    /**
     * 启动自主服务
     *
     * @param id 类名称或参数id
     */
    ApplicationService startService(String id) throws Exception;

    /**
     * 获取服务
     *
     * @param id
     * @return
     */
    ApplicationService getService(String id);

    /**
     * 获取服务列表
     *
     * @return
     */
    Collection<ApplicationService> getServices();

    /**
     * 业务服务
     *
     * @return
     */
    IBizService getBizService();

    /**
     * 获取缓存接口
     *
     * @param name
     * @param <T>
     * @return
     */
    <T> Cache<Serializable, T> getCache(String name);

    /**
     * 删除缓存
     *
     * @param name
     */
    void removeCache(String name);

    /**
     * 获取主数据库的连接
     *
     * @return
     */
    DataSource getDataSource();

    /**
     * 获取指定数据库id的连接
     *
     * @return
     */
    DataSource getDataSource(String dbId);

    /**
     * 发布消息
     *
     * @param topic
     * @param data
     * @param toUsers
     */
    void publish(String topic, Serializable data, String... toUsers);

    /**
     * 获取动态任务服务
     * @return
     */
    ICustomJob getCustomJob();
}
