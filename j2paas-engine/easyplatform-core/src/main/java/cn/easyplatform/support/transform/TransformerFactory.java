/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.transform;

import cn.easyplatform.support.transform.csv.CsvToTableTransformer;
import cn.easyplatform.support.transform.excel.ExcelToTableTransformer;
import cn.easyplatform.support.transform.xml.XmlToTableTransformer;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class TransformerFactory {

	@SuppressWarnings("unchecked")
	public static <S, T> Transformer<S, T> createTransformer(int type) {
		if (type == Transformer.XML2TABLE)
			return (Transformer<S, T>) new XmlToTableTransformer();
		//if (type == Transformer.SWI2TABLE)
		//	return (Transformer<S, T>) new SwiftToTableTransformer();
		if (type == Transformer.EXCEL2TABLE)
			return (Transformer<S, T>) new ExcelToTableTransformer();
		if (type == Transformer.CSV2TABLE)
			return (Transformer<S, T>) new CsvToTableTransformer();
		return null;
	}
}
