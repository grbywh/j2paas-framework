/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.mapping;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.CommandContext;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractMappingHandler implements MappingHandler {

	private RecordContext target;

	AbstractMappingHandler(CommandContext cc, RecordContext target) {
		this.target = target;
	}

	protected void process(String expr) {
	}

	protected void setTargetValue(String name, Object value) {
		target.setValue(name, value);
	}

	protected Object getTargetValue(String name) {
		return target.getValue(name);
	}

	protected abstract Object getSourceValue(String name);
}
