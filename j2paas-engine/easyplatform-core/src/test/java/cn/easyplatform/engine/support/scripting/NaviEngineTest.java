/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.support.scripting;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.support.scripting.ExpressionEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class NaviEngineTest extends AbstractScriptEngineTest {

	public void test1() {
		CommandContext cc = new CommandContext();
		String script = Streams.readAndClose(Streams.utf8r(getClass()
				.getResourceAsStream("navi.js")));
		RecordContext rc = new RecordContext(createRecord(),
				createSystemVariables(), createUserVariables());
		ExpressionEngine engine = ScriptEngineFactory.createExpressionEngine();
		System.out.println(engine.eval(cc, script, rc));
	}
}
