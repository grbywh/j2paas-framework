/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.page;

import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Dumps;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PageTest {
	@Test
	public void testJava2Xml() {
		PageBean pb = new PageBean();
		pb.setWidth(600);
		pb.setHeight(450);
		EventLogic el = new EventLogic();
		el.setContent("${name} = \"帅呆了!\";");
		pb.setOnLoad(el);
		el = new EventLogic();
		el.setId("WZ0020");
		pb.setOnOk(el);
		pb.setAjax("<web></web>");
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/page");
			File file = new File(url.getFile() + "/page1.xml");
			TransformerFactory.newInstance().transformToXml(pb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testXml2Java() {
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/page/page1.xml");
			PageBean lb = TransformerFactory.newInstance().transformFromXml(
					PageBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
