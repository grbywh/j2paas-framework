
package cn.easyplatform.report;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AbstractReportTest {

	public Connection getConn() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/granite-biz", "granite",
				"1qazxsw2");
	}
}
