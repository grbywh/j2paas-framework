/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Messagebox;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ActionEvent extends Event {

	private static final long serialVersionUID = 1L;

	private int actionFlag = -1;

	private String id;

	public ActionEvent(Event event, String id) {
		super(event.getName(), event.getTarget());
		if (event.getName().equals(Messagebox.ON_OK)
				|| event.getName().equals(Messagebox.ON_IGNORE))
			this.actionFlag = 1;
		this.id = id;
	}

	public int getActionFlag() {
		return actionFlag;
	}

	public String getId() {
		return id;
	}
}
