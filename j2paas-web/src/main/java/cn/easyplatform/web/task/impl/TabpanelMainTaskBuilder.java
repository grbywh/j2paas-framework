/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TabpanelMainTaskBuilder extends AbstractMainTaskBuilder<Idspace> {

    private Tab tab;

    private Tabpanel panel;

    /**
     * @param container
     * @param taskId
     * @param pv
     */
    public TabpanelMainTaskBuilder(Component container, String taskId,
                                   AbstractPageVo pv) {
        super(container, taskId, pv);
    }

    @Override
    public void build() {
        super.build();
        buildTitleButton(apv.getTitile());
        buildPanel().appendChild(idSpace);
    }

    /**
     * @param title
     */
    protected void buildTitleButton(String title) {
        tab = new Tab();
        tab.setLabel(title);
        tab.setClosable(true);
        tab.setAutoClose(false);
        PageUtils.setTaskIcon(tab, apv.getImage());
        tab.setEvent("close()");
        tab.setId(getId());
        tab.setValue(this);
        PageUtils.addEventListener(this, Events.ON_CLOSE, tab);
        ((Tabbox) container).getTabs().appendChild(tab);
        ((Tabbox) container).setSelectedTab(tab);
    }

    /**
     * @return
     */
    protected Component buildPanel() {
        panel = new Tabpanel();
        ((Tabbox) container).getTabpanels().appendChild(panel);
        return panel;
    }

    @Override
    public void close(boolean normal) {
        if (!normal) {
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(getId(), null));
            if (!resp.isSuccess()) {
                MessageBox.showMessage(resp);
                return;
            }
        }
        WebUtils.removeTask(getId());
        clear();
        idSpace.detach();
        if (tab != null) {
            tab.setAutoClose(true);
            tab.close();
        }
        if (panel != null)
            panel.detach();
        idSpace = null;
        tab = null;
        panel = null;
        int size = ((Tabbox) container).getTabs().getChildren().size();
        if (size > 0)// 返回到第一个
            ((Tabbox) container).setSelectedIndex(size - 1);
    }

}
