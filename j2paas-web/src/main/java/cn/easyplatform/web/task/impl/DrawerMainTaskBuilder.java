/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.messages.vos.VisibleVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zkmax.zul.Drawer;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.impl.XulElement;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DrawerMainTaskBuilder extends AbstractMainTaskBuilder<Idspace> {

    private EventListener<OpenEvent> el = event -> {
        if (!event.isOpen()) {
            if (idSpace == null)
                return;
            close(false);
        }
    };

    public DrawerMainTaskBuilder(Component container, String taskId, AbstractPageVo apv) {
        super(container, taskId, apv);
    }

    @Override
    public void build() {
        Drawer drawer = (Drawer) container.getPage().getFellowIfAny("__drawer__");
        drawer.getChildren().clear();
        super.build();
        drawer.appendChild(idSpace);
        String position = ((VisibleVo) apv).getPosition();
        if (Strings.isBlank(position))
            position = "right";
        drawer.setTitle(apv.getTitile());
        drawer.setPosition(position);
        drawer.addEventListener(Events.ON_OPEN, el);
        drawer.open();
    }

    @Override
    public void close(boolean normal) {
        if (!normal) {
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(getId(), null));
            if (!resp.isSuccess()) {
                MessageBox.showMessage(resp);
                return;
            }
        }
        WebUtils.removeTask(getId());
        clear();
        idSpace.getParent().removeEventListener(Events.ON_OPEN, el);
        ((Drawer) idSpace.getParent()).close();
        idSpace.detach();
        idSpace = null;
    }
}
