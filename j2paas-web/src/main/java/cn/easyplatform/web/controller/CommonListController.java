/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.GetListRequestMessage;
import cn.easyplatform.messages.vos.GetListVo;
import cn.easyplatform.messages.vos.component.ListPageVo;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.ext.QueryCallback;
import cn.easyplatform.web.service.ServiceLocator;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommonListController extends SelectorComposer<Window> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2907900333635070783L;

	@Wire("listbox")
	private Listbox listbox;

	@Wire("paging")
	private Paging paging;

	private QueryCallback cb;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		cb = (QueryCallback) Executions.getCurrent().getArg().get("cb");
		GetListVo gv = new GetListVo(cb.getDbId(), cb.getQuery());
		gv.setFilter(cb.getFilter());
		gv.setPageSize(cb.getPageSize());
		gv.setOrderBy(cb.getOrderBy());
		gv.setGetCount(true);
		GetListRequestMessage req = new GetListRequestMessage(cb.getId(), gv);
		ComponentService cs = ServiceLocator
				.lookup(ComponentService.class);
		IResponseMessage<?> resp = cs.getList(req);
		if (resp.isSuccess()) {
			ListPageVo pv = (ListPageVo) resp.getBody();
			paging.setTotalSize(pv.getTotalCount());
			paging.setPageSize(cb.getPageSize());
			paging.setDetailed(true);
			paging.setMold("os");
			if (!Strings.isBlank(cb.getHeaders())) {
				String[] headers = cb.getHeaders().split(",");
				Listhead head = new Listhead();
				for (String tmp : headers) {
					Listheader header = new Listheader(tmp);
					header.setParent(head);
				}
				head.setParent(listbox);
			}
			draw(pv.getData());
		} else
			Clients.wrongValue(listbox, (String) resp.getBody());
	}

	private void draw(List<FieldVo[]> list) {
		listbox.getItems().clear();
		for (FieldVo[] fvs : list) {
			Listitem li = new Listitem();
			li.setValue(fvs);
			for (int i = 0; i < fvs.length; i++) {
				Listcell c = new Listcell(fvs[i].getValue() == null ? ""
						: fvs[i].getValue().toString());
				c.setParent(li);
			}
			li.setParent(listbox);
		}
		listbox.getItemAtIndex(0).setSelected(true);
	}

	@Listen("onPaging=#paging")
	public void paging(PagingEvent evt) {
		GetListVo gv = new GetListVo(cb.getDbId(), cb.getQuery());
		gv.setFilter(cb.getFilter());
		gv.setPageSize(cb.getPageSize());
		gv.setOrderBy(cb.getOrderBy());
		gv.setGetCount(false);
		gv.setPageNo(evt.getActivePage() + 1);
		ComponentService cs = ServiceLocator
				.lookup(ComponentService.class);
		GetListRequestMessage req = new GetListRequestMessage(cb.getId(), gv);
		IResponseMessage<?> resp = cs.getList(req);
		if (resp.isSuccess()) {
			@SuppressWarnings("unchecked")
			List<FieldVo[]> list = (List<FieldVo[]>) resp.getBody();
			draw(list);
		} else
			Clients.wrongValue(listbox, (String) resp.getBody());
	}

	@Listen("onDoubleClick=#listbox;onOK=window")
	public void select() {
		if (listbox.getSelectedCount() > 0) {
			FieldVo[] fvs = listbox.getSelectedItem().getValue();
			cb.call(fvs);
			listbox.getRoot().detach();
		}
	}

	@Listen("onCancel=window")
	public void close() {
		listbox.getRoot().detach();
	}
}
