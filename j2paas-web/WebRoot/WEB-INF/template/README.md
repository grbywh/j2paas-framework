### 前端模板目录空间

- 目录结构
  
  - commonTemplates 系统提供的公共页面模板库
    - Desktop-Login-001.jar PC端登录页面模板，编号001
    - Desktop-Login-002.jar PC端登录页面模板，编号002
    - Desktop-Login-003.jar PC端登录页面模板，编号003
    - Desktop-Login-004.jar PC端登录页面模板，编号004
    - Desktop-Main-001.jar PC端管理主页模板，编号001
    - Mobile-Login-001.jar 移动端登录页面模板，编号001
    - Mobile-Login-002.jar 移动端登录页面模板，编号002
    - Mobile-Login-003.jar 移动端登录页面模板，编号003
    - Mobile-Login-004.jar 移动端登录页面模板，编号004     
    - Mobile-Login-005.jar 移动端登录页面模板，编号005
    - Mobile-Login-006.jar 移动端登录页面模板，编号006
    - Mobile-Login-007.jar 移动端登录页面模板，编号007
    - Mobile-Login-008.jar 移动端登录页面模板，编号008
    - Mobile-Login-009.jar 移动端登录页面模板，编号009
    - Mobile-Login-010.jar 移动端登录页面模板，编号010
    - Mobile-Login-011.jar 移动端登录页面模板，编号011
    - Mobile-Main-001.jar 移动端管理主页模板，编号001
    - Mobile-Main-002.jar 移动端管理主页模板，编号002 
    - Mobile-Main-003.jar 移动端管理主页模板，编号003
    - Mobile-Main-004.jar 移动端管理主页模板，编号004
    - Mobile-Main-005.jar 移动端管理主页模板，编号005
      
  - xxxxxx 项目页面模板,xxxxx表示运行项目的id
    - d0 PC端登录页面引用的资源文件目录
    - d1 PC端管理主页引用的资源文件目录
    - m0 移动端登录页面引用的资源文件目录
    - m1 移动端管理主页引用的资源文件目录
    - public 公共资源文件目录
    - templates 项目模板库